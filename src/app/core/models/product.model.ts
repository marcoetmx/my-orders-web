export interface Product {
  id: number;
  sku: string;
  barcode: string;
  urlimage: string;
  createdAt: string;
  updatedAt: string;
}
