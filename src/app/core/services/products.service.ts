import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Product } from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http: HttpClient) { }

  getAllProducts(){
    return this.http.get<Product[]>(`${environment.url_api}/items`);
  }

  createProduct(product: Product){
    return this.http.post<Product>(`${environment.url_api}/items`, product);
  }

  getProductbyId(id: string){
    return this.http.get<Product>(`${environment.url_api}/items/${id}`);
  }

  updateProduct(product: Product){
    return this.http.patch<Product>(`${environment.url_api}/items/${product.id}`, product);
  }

  deleteProduct(id: string){
    return this.http.delete(`${environment.url_api}/items/${id}`);
  }
}
