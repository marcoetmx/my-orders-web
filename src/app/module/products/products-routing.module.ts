import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ProductsCreateComponent } from "./components/products-create/products-create.component";
import { ProductsListComponent } from "./components/products-list/products-list.component";

const routes: Routes = [
  {
    path: '', component: ProductsListComponent
  },
  {
    path: 'create', component: ProductsCreateComponent
  },
  {
    path: ':id', component: ProductsCreateComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class ProductosRoutingModule { }
