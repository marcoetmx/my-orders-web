import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Product } from 'src/app/core/models/product.model';
import { ProductsService } from 'src/app/core/services/products.service';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {

  products: Product[] = [];
  displayedColumns: string[] = ['sku', 'description', 'barcode', 'updatedAt']
  dataSource = new MatTableDataSource<Product>(this.products)

  constructor(private productsService: ProductsService, private  router: Router) { }

  @ViewChild(MatPaginator)
  paginator!: MatPaginator;

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.fetchProducts()
  }

  fetchProducts(){
    this.productsService.getAllProducts()
     .subscribe(dataProducts => {
       this.products = dataProducts
       this.dataSource.data = this.products
     })
  }

  viewProduc(productSelect: Product){
    console.log(productSelect);
    this.router.navigate([`products/${productSelect.id}`])
  }
}
