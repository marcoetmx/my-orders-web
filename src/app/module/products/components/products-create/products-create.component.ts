import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { retry } from 'rxjs/operators';
import { Modes } from 'src/app/core/models/mode.model';
import { Product } from 'src/app/core/models/product.model';
import { ProductsService } from 'src/app/core/services/products.service';

@Component({
  selector: 'app-products-create',
  templateUrl: './products-create.component.html',
  styleUrls: ['./products-create.component.scss'],
})
export class ProductsCreateComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private activeRoute: ActivatedRoute,
    private productService: ProductsService,
    private router: Router
  ) {
    this.buildForm();
  }

  form!: FormGroup;
  id: string = '';
  title: string = 'Crear';
  editProduct!: Product;
  ModesEnum = Modes;
  currentMode = this.ModesEnum.Create;

  ngOnInit(): void {
    this.getIdProduct();
  }

  saveProduct(event: Event) {
    event.preventDefault();

    if (this.form.valid && this.currentMode == this.ModesEnum.Create) {
      const product = this.form.value;
      this.productService.createProduct(product).subscribe((newProduct) => {
        this.router.navigate(['./products']);
      });
    } else if (this.form.valid && this.currentMode == this.ModesEnum.Edit) {
      const updateProduct: Product = this.form.value;
      updateProduct.id = +this.id;
      this.productService
        .updateProduct(updateProduct)
        .subscribe((newProduct) => {
          this.router.navigate(['./products']);
        });
    }
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      sku: ['', [Validators.required]],
      description: ['', [Validators.required]],
      barcode: [''],
      urlimage: [''],
    });
  }

  redirect() {
    this.router.navigate(['./products']);
  }

  editMode() {
    this.currentMode = this.ModesEnum.Edit;
  }

  private getIdProduct() {
    this.activeRoute.params.subscribe((params: Params) => {
      this.id = params.id;
      if (this.id && this.id != '') {
        this.productService
          .getProductbyId(this.id)
          .subscribe((product: Product) => {
            this.currentMode = this.ModesEnum.Detail;
            this.editProduct = product;
            this.form.patchValue(product);
          });
      }
    });
  }

  getStringMode() {
    if (this.currentMode == this.ModesEnum.Create) {
      return 'Crear';
    } else if (this.currentMode == this.ModesEnum.Detail) {
      return 'Detalle';
    }

    return 'Editar';
  }

  delete() {
    if (this.id != '') {
      this.productService.deleteProduct(this.id).subscribe((response: any) => {
        this.router.navigate(['./products']);
      });
    }
  }
}
